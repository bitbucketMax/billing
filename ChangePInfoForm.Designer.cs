﻿namespace Billing
{
    partial class ChangePInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbhome = new System.Windows.Forms.Label();
            this.lbstreet = new System.Windows.Forms.Label();
            this.ltown = new System.Windows.Forms.Label();
            this.txtHome = new System.Windows.Forms.TextBox();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdChange = new System.Windows.Forms.Button();
            this.lbadr = new System.Windows.Forms.Label();
            this.lblFamiliya = new System.Windows.Forms.Label();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.txtfamilia = new System.Windows.Forms.TextBox();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtothestvo = new System.Windows.Forms.TextBox();
            this.lbname = new System.Windows.Forms.Label();
            this.lbOtchestvo = new System.Windows.Forms.Label();
            this.lbresult = new System.Windows.Forms.Label();
            this.lbresfamilya = new System.Windows.Forms.Label();
            this.lbresname = new System.Windows.Forms.Label();
            this.lbresoch = new System.Windows.Forms.Label();
            this.lbresadr = new System.Windows.Forms.Label();
            this.txtoldpasswd = new System.Windows.Forms.TextBox();
            this.txtnewpaswd = new System.Windows.Forms.TextBox();
            this.lbnewpaswd = new System.Windows.Forms.Label();
            this.lbnewpasswd = new System.Windows.Forms.Label();
            this.lbrespasswd = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbhome
            // 
            this.lbhome.AutoSize = true;
            this.lbhome.Location = new System.Drawing.Point(68, 161);
            this.lbhome.Name = "lbhome";
            this.lbhome.Size = new System.Drawing.Size(30, 13);
            this.lbhome.TabIndex = 21;
            this.lbhome.Text = "Дом";
            // 
            // lbstreet
            // 
            this.lbstreet.AutoSize = true;
            this.lbstreet.Location = new System.Drawing.Point(64, 135);
            this.lbstreet.Name = "lbstreet";
            this.lbstreet.Size = new System.Drawing.Size(39, 13);
            this.lbstreet.TabIndex = 20;
            this.lbstreet.Text = "Улица";
            // 
            // ltown
            // 
            this.ltown.AutoSize = true;
            this.ltown.Location = new System.Drawing.Point(64, 109);
            this.ltown.Name = "ltown";
            this.ltown.Size = new System.Drawing.Size(37, 13);
            this.ltown.TabIndex = 19;
            this.ltown.Text = "Город";
            // 
            // txtHome
            // 
            this.txtHome.Location = new System.Drawing.Point(109, 161);
            this.txtHome.Name = "txtHome";
            this.txtHome.Size = new System.Drawing.Size(209, 20);
            this.txtHome.TabIndex = 18;
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(109, 135);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(209, 20);
            this.txtStreet.TabIndex = 17;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(317, 275);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(135, 39);
            this.cmdCancel.TabIndex = 16;
            this.cmdCancel.Text = "Возврат в меню клиетна";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdChange
            // 
            this.cmdChange.Location = new System.Drawing.Point(82, 275);
            this.cmdChange.Name = "cmdChange";
            this.cmdChange.Size = new System.Drawing.Size(124, 39);
            this.cmdChange.TabIndex = 15;
            this.cmdChange.Text = "Изменить данные";
            this.cmdChange.UseVisualStyleBackColor = true;
            this.cmdChange.Click += new System.EventHandler(this.cmdChange_Click);
            // 
            // lbadr
            // 
            this.lbadr.AutoSize = true;
            this.lbadr.Location = new System.Drawing.Point(12, 109);
            this.lbadr.Name = "lbadr";
            this.lbadr.Size = new System.Drawing.Size(38, 13);
            this.lbadr.TabIndex = 14;
            this.lbadr.Text = "Адрес";
            // 
            // lblFamiliya
            // 
            this.lblFamiliya.AutoSize = true;
            this.lblFamiliya.Location = new System.Drawing.Point(37, 26);
            this.lblFamiliya.Name = "lblFamiliya";
            this.lblFamiliya.Size = new System.Drawing.Size(56, 13);
            this.lblFamiliya.TabIndex = 13;
            this.lblFamiliya.Text = "Фамилия";
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(109, 109);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(209, 20);
            this.txtTown.TabIndex = 12;
            // 
            // txtfamilia
            // 
            this.txtfamilia.Location = new System.Drawing.Point(109, 19);
            this.txtfamilia.Name = "txtfamilia";
            this.txtfamilia.Size = new System.Drawing.Size(209, 20);
            this.txtfamilia.TabIndex = 11;
            // 
            // txtname
            // 
            this.txtname.Location = new System.Drawing.Point(109, 45);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(209, 20);
            this.txtname.TabIndex = 11;
            // 
            // txtothestvo
            // 
            this.txtothestvo.Location = new System.Drawing.Point(109, 71);
            this.txtothestvo.Name = "txtothestvo";
            this.txtothestvo.Size = new System.Drawing.Size(209, 20);
            this.txtothestvo.TabIndex = 11;
            // 
            // lbname
            // 
            this.lbname.AutoSize = true;
            this.lbname.Location = new System.Drawing.Point(64, 48);
            this.lbname.Name = "lbname";
            this.lbname.Size = new System.Drawing.Size(29, 13);
            this.lbname.TabIndex = 13;
            this.lbname.Text = "Имя";
            // 
            // lbOtchestvo
            // 
            this.lbOtchestvo.AutoSize = true;
            this.lbOtchestvo.Location = new System.Drawing.Point(39, 71);
            this.lbOtchestvo.Name = "lbOtchestvo";
            this.lbOtchestvo.Size = new System.Drawing.Size(54, 13);
            this.lbOtchestvo.TabIndex = 13;
            this.lbOtchestvo.Text = "Отчество";
            // 
            // lbresult
            // 
            this.lbresult.AutoSize = true;
            this.lbresult.Location = new System.Drawing.Point(12, 217);
            this.lbresult.Name = "lbresult";
            this.lbresult.Size = new System.Drawing.Size(0, 13);
            this.lbresult.TabIndex = 22;
            // 
            // lbresfamilya
            // 
            this.lbresfamilya.AutoSize = true;
            this.lbresfamilya.Location = new System.Drawing.Point(340, 26);
            this.lbresfamilya.Name = "lbresfamilya";
            this.lbresfamilya.Size = new System.Drawing.Size(0, 13);
            this.lbresfamilya.TabIndex = 23;
            // 
            // lbresname
            // 
            this.lbresname.AutoSize = true;
            this.lbresname.Location = new System.Drawing.Point(340, 55);
            this.lbresname.Name = "lbresname";
            this.lbresname.Size = new System.Drawing.Size(0, 13);
            this.lbresname.TabIndex = 23;
            // 
            // lbresoch
            // 
            this.lbresoch.AutoSize = true;
            this.lbresoch.Location = new System.Drawing.Point(340, 78);
            this.lbresoch.Name = "lbresoch";
            this.lbresoch.Size = new System.Drawing.Size(0, 13);
            this.lbresoch.TabIndex = 23;
            // 
            // lbresadr
            // 
            this.lbresadr.AutoSize = true;
            this.lbresadr.Location = new System.Drawing.Point(340, 142);
            this.lbresadr.Name = "lbresadr";
            this.lbresadr.Size = new System.Drawing.Size(0, 13);
            this.lbresadr.TabIndex = 23;
            // 
            // txtoldpasswd
            // 
            this.txtoldpasswd.Location = new System.Drawing.Point(109, 200);
            this.txtoldpasswd.Name = "txtoldpasswd";
            this.txtoldpasswd.Size = new System.Drawing.Size(209, 20);
            this.txtoldpasswd.TabIndex = 18;
            // 
            // txtnewpaswd
            // 
            this.txtnewpaswd.Location = new System.Drawing.Point(109, 226);
            this.txtnewpaswd.Name = "txtnewpaswd";
            this.txtnewpaswd.Size = new System.Drawing.Size(209, 20);
            this.txtnewpaswd.TabIndex = 18;
            // 
            // lbnewpaswd
            // 
            this.lbnewpaswd.AutoSize = true;
            this.lbnewpaswd.Location = new System.Drawing.Point(19, 200);
            this.lbnewpaswd.Name = "lbnewpaswd";
            this.lbnewpaswd.Size = new System.Drawing.Size(84, 13);
            this.lbnewpaswd.TabIndex = 21;
            this.lbnewpaswd.Text = "Старый пароль";
            // 
            // lbnewpasswd
            // 
            this.lbnewpasswd.AutoSize = true;
            this.lbnewpasswd.Location = new System.Drawing.Point(23, 229);
            this.lbnewpasswd.Name = "lbnewpasswd";
            this.lbnewpasswd.Size = new System.Drawing.Size(80, 13);
            this.lbnewpasswd.TabIndex = 21;
            this.lbnewpasswd.Text = "Новый пароль";
            // 
            // lbrespasswd
            // 
            this.lbrespasswd.AutoSize = true;
            this.lbrespasswd.Location = new System.Drawing.Point(343, 206);
            this.lbrespasswd.Name = "lbrespasswd";
            this.lbrespasswd.Size = new System.Drawing.Size(0, 13);
            this.lbrespasswd.TabIndex = 24;
            // 
            // ChangePInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 339);
            this.Controls.Add(this.lbrespasswd);
            this.Controls.Add(this.lbresadr);
            this.Controls.Add(this.lbresoch);
            this.Controls.Add(this.lbresname);
            this.Controls.Add(this.lbresfamilya);
            this.Controls.Add(this.lbresult);
            this.Controls.Add(this.lbnewpasswd);
            this.Controls.Add(this.lbnewpaswd);
            this.Controls.Add(this.lbhome);
            this.Controls.Add(this.lbstreet);
            this.Controls.Add(this.ltown);
            this.Controls.Add(this.txtnewpaswd);
            this.Controls.Add(this.txtoldpasswd);
            this.Controls.Add(this.txtHome);
            this.Controls.Add(this.txtStreet);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdChange);
            this.Controls.Add(this.lbadr);
            this.Controls.Add(this.lbOtchestvo);
            this.Controls.Add(this.lbname);
            this.Controls.Add(this.lblFamiliya);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.txtothestvo);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.txtfamilia);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ChangePInfoForm";
            this.Text = "Изменение личных данных";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChangePInfoForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbhome;
        private System.Windows.Forms.Label lbstreet;
        private System.Windows.Forms.Label ltown;
        private System.Windows.Forms.TextBox txtHome;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdChange;
        private System.Windows.Forms.Label lbadr;
        private System.Windows.Forms.Label lblFamiliya;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.TextBox txtfamilia;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TextBox txtothestvo;
        private System.Windows.Forms.Label lbname;
        private System.Windows.Forms.Label lbOtchestvo;
        private System.Windows.Forms.Label lbresult;
        private System.Windows.Forms.Label lbresfamilya;
        private System.Windows.Forms.Label lbresname;
        private System.Windows.Forms.Label lbresoch;
        private System.Windows.Forms.Label lbresadr;
        private System.Windows.Forms.TextBox txtoldpasswd;
        private System.Windows.Forms.TextBox txtnewpaswd;
        private System.Windows.Forms.Label lbnewpaswd;
        private System.Windows.Forms.Label lbnewpasswd;
        private System.Windows.Forms.Label lbrespasswd;
    }
}