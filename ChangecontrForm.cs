﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class ChangecontrForm : Form
    {
        private string yes = "yes", no = "no", pcall, psms, pinternet;
        private object vashid = "ваш id: ";
        private object vashnumb = "ваш номер: ";
        private string idtarif = "id Тарифа";
        private string pricmin = "1 минута, руб";
        private string prisms = "1 смс, руб";
        private string primb = " 1 мб, руб";
        private string pname = "Провайдер";
        public string tariffselected;
        public ChangecontrForm()
        {
            InitializeComponent();
        }

        private void ChtariffBtn_Click(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            int indexcontract = contractmenu.MMform.logformcopy.control.ExistContract(contractmenu.MMform.numbcontrselected);
            int indextariff = contractmenu.MMform.logformcopy.control.ExistTariff(tariffselected);
            lbchangetariffres.Text = contractmenu.MMform.logformcopy.control.contracts[indexcontract].Changetariff(contractmenu.MMform.logformcopy.control.tariffs[indextariff]);
            contractmenu.MMform.logformcopy.control.Save();
        }

        private void listtariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChtariffBtn.Enabled = true;
            if (!String.IsNullOrEmpty(listtariff.FocusedItem.SubItems[0].Text))
                tariffselected = listtariff.FocusedItem.SubItems[0].Text;
        }

        private void ChangecontrForm_Load(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            listtariff.Columns.Add(idtarif);
            listtariff.Columns.Add(pricmin);
            listtariff.Columns.Add(prisms);
            listtariff.Columns.Add(primb);
            listtariff.Columns.Add(pname);

            for (int i = 0; i < contractmenu.MMform.logformcopy.control.tariffs.Count; i++)
            {
                listtariff.Items.Add(new ListViewItem(contractmenu.MMform.logformcopy.control.tariffs[i].getArr()));
            }
            listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void ChangecontrForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            contractmenu.MMform.logformcopy.control.Save();
            this.Owner.Show();
            this.Hide();
        }

        private void btnservice_Click(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            if (podcall.Checked)
                pcall = yes;
            else pcall = no;
            if (podsms.Checked)
                psms = yes;
            else psms = no;
            if (podinternet.Checked)
                pinternet = yes;
            else pinternet = no;
            int indexcontract = contractmenu.MMform.logformcopy.control.ExistContract(contractmenu.MMform.numbcontrselected);
            lbserviceschangeres.Text = contractmenu.MMform.logformcopy.control.contracts[indexcontract].Changesrevices(pcall, psms, pinternet);
        }

        private void btnchangenumb_Click(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            int indexcontract = contractmenu.MMform.logformcopy.control.ExistContract(contractmenu.MMform.numbcontrselected);
            lbnumchangeres.Text = contractmenu.MMform.logformcopy.control.ChangePhoneNumber(contractmenu.MMform.numbcontrselected, numphone.Value.ToString());
            contractmenu.MMform.numbcontrselected = contractmenu.MMform.logformcopy.control.contracts[indexcontract].phonenumber;
        }

        private void btnpasswd_Click(object sender, EventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            int indexcontract = contractmenu.MMform.logformcopy.control.ExistContract(contractmenu.MMform.numbcontrselected);
            lbpasswdchangeres.Text = contractmenu.MMform.logformcopy.control.contracts[indexcontract].ChangePassword(txtoldpasswd.Text, txtpasswd.Text);
        }

        private void listtariff_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ContractMenu contractmenu = this.Owner as ContractMenu;
            List<string[]> ltariff = new List<string[]>();
            foreach (var strtariff in contractmenu.MMform.logformcopy.control.tariffs)
                ltariff.Add(strtariff.getArr());
            if (listtariff.Tag.Equals("-1"))
                listtariff.Tag = "1";
            else listtariff.Tag = "-1";
            ltariff.Sort(delegate(string[] a1, string[] a2)
            {
                if (e.Column != 4)
                {
                    int i1 = Convert.ToInt32(a1[e.Column]),
                        i2 = Convert.ToInt32(a2[e.Column]);
                    if (i1 > i2)
                        return (listtariff.Tag.Equals("1") ? 1 : -1);
                    else if (i1 < i2)
                        return (listtariff.Tag.Equals("1") ? -1 : 1);
                    else return 0;
                }
                else
                    if ((String.Compare(a1[e.Column], a2[e.Column]) == 1))
                        return (listtariff.Tag.Equals("1") ? 1 : -1);
                    else if ((String.Compare(a1[e.Column], a2[e.Column]) == -1))
                        return (listtariff.Tag.Equals("1") ? -1 : 1);
                    else return 0;

            });

            listtariff.Items.Clear();
            foreach (var strmastariff in ltariff)
                listtariff.Items.Add(new ListViewItem(strmastariff));
            listtariff.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }



        }
}


