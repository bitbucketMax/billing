﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Billing
{
    public partial class ContractMenu : Form
    {
        private string numb = "номер";
        private string bill = "Баланс";
        private string dateconclude = "дата закллючения контракта";
        private string podcall = "звонки";
        private string podsms = "sms";
        private string podinternet = "internet";
        private string tariff = "тариф";
        private string provider = "Провайдер";
        public MainMenu MMform;
        public ContractMenu()
        {
            InitializeComponent();
        }

        private void cmdBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Owner.Show();
        }

        private void ContractMenu_Load(object sender, EventArgs e)
        {
            MainMenu MainMenuForm = this.Owner as MainMenu;
            listContract.Clear();
            listContract.Columns.Add(numb);
            listContract.Columns.Add(bill);
            listContract.Columns.Add(dateconclude);
            listContract.Columns.Add(podcall);
            listContract.Columns.Add(podsms);
            listContract.Columns.Add(podinternet);
            listContract.Columns.Add(tariff);
            listContract.Columns.Add(provider);
            int indexcontract = MainMenuForm.logformcopy.control.ExistContract(MainMenuForm.numbcontrselected);
            listContract.Items.Add(new ListViewItem(MainMenuForm.logformcopy.control.contracts[indexcontract].getArr()));
            listContract.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void ContractMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void cmdDelete_Click(object sender, EventArgs e)
        {
            string message = "Вы хотите расторгнуть этот контракт. Вы уверены?";
            string caption = "Расторжение контракта";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(this, message, caption, buttons,
            MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (result == DialogResult.Yes)
            {
                MainMenu MainMenuForm = this.Owner as MainMenu;
                this.Hide();
                MainMenuForm.logformcopy.control.DelContract(MainMenuForm.numbcontrselected);
                MainMenuForm.logformcopy.control.Save();
                this.Owner.Show();
            }
            
           

        }

        private void cmdPay_Click(object sender, EventArgs e)
        {
            MainMenu MainMenuForm = this.Owner as MainMenu;
            MMform = MainMenuForm;
            PayForm payf = new PayForm();
            payf.Owner = this;
            payf.Show();
            this.Hide();
        }

        private void cmdDetail_Click(object sender, EventArgs e)
        {
            MainMenu MainMenuForm = this.Owner as MainMenu;
            MMform = MainMenuForm;
            Detalisationform detf = new Detalisationform();
            detf.Owner = this;
            detf.Show();
            this.Hide();
        }

        private void cmdChange_Click(object sender, EventArgs e)
        {
            MainMenu MainMenuForm = this.Owner as MainMenu;
            MMform = MainMenuForm;
            ChangecontrForm cform = new ChangecontrForm();
            cform.Owner = this;
            cform.Show();
            this.Hide();

        }
    }
}
