﻿namespace Billing
{
    partial class ConcludeContractForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbpasswd = new System.Windows.Forms.Label();
            this.lbservenabled = new System.Windows.Forms.Label();
            this.podcall = new System.Windows.Forms.CheckBox();
            this.podsms = new System.Windows.Forms.CheckBox();
            this.podinternet = new System.Windows.Forms.CheckBox();
            this.txtpasswd = new System.Windows.Forms.TextBox();
            this.ConcludeBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.lbres = new System.Windows.Forms.Label();
            this.lbnumb = new System.Windows.Forms.Label();
            this.listtariff = new System.Windows.Forms.ListView();
            this.lbtariff = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbpasswd
            // 
            this.lbpasswd.AutoSize = true;
            this.lbpasswd.Location = new System.Drawing.Point(419, 136);
            this.lbpasswd.Name = "lbpasswd";
            this.lbpasswd.Size = new System.Drawing.Size(45, 13);
            this.lbpasswd.TabIndex = 0;
            this.lbpasswd.Text = "Пароль";
            // 
            // lbservenabled
            // 
            this.lbservenabled.AutoSize = true;
            this.lbservenabled.Location = new System.Drawing.Point(432, 11);
            this.lbservenabled.Name = "lbservenabled";
            this.lbservenabled.Size = new System.Drawing.Size(122, 13);
            this.lbservenabled.TabIndex = 0;
            this.lbservenabled.Text = "Подключаемые услуги";
            // 
            // podcall
            // 
            this.podcall.AutoSize = true;
            this.podcall.Location = new System.Drawing.Point(435, 37);
            this.podcall.Name = "podcall";
            this.podcall.Size = new System.Drawing.Size(62, 17);
            this.podcall.TabIndex = 1;
            this.podcall.Text = "звонок";
            this.podcall.UseVisualStyleBackColor = true;
            // 
            // podsms
            // 
            this.podsms.AutoSize = true;
            this.podsms.Location = new System.Drawing.Point(435, 60);
            this.podsms.Name = "podsms";
            this.podsms.Size = new System.Drawing.Size(46, 17);
            this.podsms.TabIndex = 1;
            this.podsms.Text = "смс";
            this.podsms.UseVisualStyleBackColor = true;
            // 
            // podinternet
            // 
            this.podinternet.AutoSize = true;
            this.podinternet.Location = new System.Drawing.Point(435, 83);
            this.podinternet.Name = "podinternet";
            this.podinternet.Size = new System.Drawing.Size(72, 17);
            this.podinternet.TabIndex = 1;
            this.podinternet.Text = "интернет";
            this.podinternet.UseVisualStyleBackColor = true;
            // 
            // txtpasswd
            // 
            this.txtpasswd.Location = new System.Drawing.Point(502, 136);
            this.txtpasswd.Name = "txtpasswd";
            this.txtpasswd.Size = new System.Drawing.Size(100, 20);
            this.txtpasswd.TabIndex = 2;
            // 
            // ConcludeBtn
            // 
            this.ConcludeBtn.Location = new System.Drawing.Point(167, 245);
            this.ConcludeBtn.Name = "ConcludeBtn";
            this.ConcludeBtn.Size = new System.Drawing.Size(135, 39);
            this.ConcludeBtn.TabIndex = 4;
            this.ConcludeBtn.Text = "Заключить договор";
            this.ConcludeBtn.UseVisualStyleBackColor = true;
            this.ConcludeBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(391, 245);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(128, 39);
            this.CancelBtn.TabIndex = 4;
            this.CancelBtn.Text = "Вернуться в меню клиента";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // lbres
            // 
            this.lbres.AutoSize = true;
            this.lbres.Location = new System.Drawing.Point(481, 178);
            this.lbres.Name = "lbres";
            this.lbres.Size = new System.Drawing.Size(0, 13);
            this.lbres.TabIndex = 6;
            // 
            // lbnumb
            // 
            this.lbnumb.AutoSize = true;
            this.lbnumb.Location = new System.Drawing.Point(474, 206);
            this.lbnumb.Name = "lbnumb";
            this.lbnumb.Size = new System.Drawing.Size(0, 13);
            this.lbnumb.TabIndex = 7;
            // 
            // listtariff
            // 
            this.listtariff.FullRowSelect = true;
            this.listtariff.Location = new System.Drawing.Point(21, 33);
            this.listtariff.Name = "listtariff";
            this.listtariff.Size = new System.Drawing.Size(377, 186);
            this.listtariff.TabIndex = 9;
            this.listtariff.Tag = "1";
            this.listtariff.UseCompatibleStateImageBehavior = false;
            this.listtariff.View = System.Windows.Forms.View.Details;
            this.listtariff.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listtariff_ColumnClick);
            this.listtariff.SelectedIndexChanged += new System.EventHandler(this.listtariff_SelectedIndexChanged);
            // 
            // lbtariff
            // 
            this.lbtariff.AutoSize = true;
            this.lbtariff.Location = new System.Drawing.Point(18, 9);
            this.lbtariff.Name = "lbtariff";
            this.lbtariff.Size = new System.Drawing.Size(91, 13);
            this.lbtariff.TabIndex = 0;
            this.lbtariff.Text = "Выберите тариф";
            // 
            // ConcludeContractForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 315);
            this.Controls.Add(this.listtariff);
            this.Controls.Add(this.lbnumb);
            this.Controls.Add(this.lbres);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.ConcludeBtn);
            this.Controls.Add(this.txtpasswd);
            this.Controls.Add(this.podinternet);
            this.Controls.Add(this.podsms);
            this.Controls.Add(this.podcall);
            this.Controls.Add(this.lbtariff);
            this.Controls.Add(this.lbservenabled);
            this.Controls.Add(this.lbpasswd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ConcludeContractForm";
            this.Text = "Заключение договора";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConcludeContractForm_FormClosing);
            this.Load += new System.EventHandler(this.ConcludeContractForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbpasswd;
        private System.Windows.Forms.Label lbservenabled;
        private System.Windows.Forms.CheckBox podcall;
        private System.Windows.Forms.CheckBox podsms;
        private System.Windows.Forms.CheckBox podinternet;
        private System.Windows.Forms.TextBox txtpasswd;
        private System.Windows.Forms.Button ConcludeBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Label lbres;
        private System.Windows.Forms.Label lbnumb;
        private System.Windows.Forms.ListView listtariff;
        private System.Windows.Forms.Label lbtariff;
    }
}