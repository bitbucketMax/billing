﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
   public class Controlclass
    {
        private string savePath;

        private const string ProvidersFileName = "Providers.csv";
        private const string ContractsFileName = "Contracts.csv";
        private const string EventsFileName = "Events.csv";
        private const string TariffFileName = "Tariff.csv";
        private const string zapvsepola = "заполните все поля";
        private string ClientFileName="Client.csv";
        public string yes = "yes";
        public string notenableserv = "используемый сервис является неподключенным";
        public string notexistserv = "данного сервиса не существет";
        private string contrisnotexist = "данного контракта не существует";
        private string dobavleno = "добавлено";
        private string stype = "sms";
        private string itype = "internet";
        private string ctype = "call";
        private string tariffnotexist = "такого тарифа не существует";
        private string clientnotexist = "такого клиента не существует";
        private string tariffexist = "такой тариф  существует";
        private string providernotexist = "данного провайдера не существует";
        private string providerexist = "такой провайдер уже существет";
        private string deleted="удалено";
        private string cantdelet="невозможно удалить последний элемент";
        private string ismeneno = "Данные успешно измененны";
       private string cantchangenumb="данный номер уже существует";
        private string mustbetensimb= "мобильный номер должен содержать 10 цифорвых знаков";
        private string emtstr="";
        private string invalidclientpasswd="неверная пара логин/пароль";
        private string invalidsimv = "использование данных символов недопустимо";
        private string emptadr = "г. , ул., д. ";
        private string substrtownstreet = "г. , ул.";
        private string substrtownhome = ", д. ";
        private string substrstreetnhome = ", ул., д. ";

        public List<Provider> providers = new List<Provider>();
        public List<Contract> contracts = new List<Contract>();
        public List<Client> clients = new List<Client>();
        public List<Event> events = new List<Event>();
        public List<Tariff> tariffs = new List<Tariff>();
       
        
        
 
        

        
       
        
        /// <summary>
        /// конструктор класса Controlclass
        /// </summary>
        /// <param name="savePath">путь для сохранения/загрузки файлов:Providers.csv, Contracts.csv,Events.csv,Tariff.csv,Client.csv.</param>
        public Controlclass(string savePath)
        {
            this.savePath = savePath;
        }
        
        /// <summary>
        /// метод для загрузки списка провайдеров из файла Providers.csv
        /// </summary>
        private void LoadProviders()
        {
            String[] prov = File.ReadAllLines(savePath + "\\" + ProvidersFileName);
            foreach(String s in prov)
            {
                providers.Add(new Provider(s));
               
            }
            
        }
        /// <summary>
        /// метод для загрузки списка контрактов из файла Contracts.csv 
        /// </summary>
        private void LoadContracts()
        {
            String[] cont = File.ReadAllLines(savePath + "\\" + ContractsFileName);
            foreach (String s in cont)
            {
                String[] arr = s.Split(';');
                contracts.Add(new Contract(Convert.ToDouble(arr[0]), Convert.ToInt32(arr[1]), arr[2], arr[3],arr[7],arr[8],arr[9],arr[10]));//cоздаем контракт (bill,idcontract,passwd,phonenumber)
                LinkContractvsall(arr[4], arr[5], arr[6]);



            }
        }
       /// <summary>
        /// метод для загрузки событий из файла Events.csv
       /// </summary>
        private void LoadEvents()
        {
            String[] ev = File.ReadAllLines(savePath + "\\" + EventsFileName);
            foreach (String s in ev)
            {
                String[] arr = s.Split(';');
                events.Add(new Event(arr[0],arr[1],arr[2],arr[3],arr[4]));
                int idcontr = Convert.ToInt32(arr[5]);
                int i = 0;
                bool flag = true;
                while (i < contracts.Count&&flag)
                {
                    if (contracts[i].idcontract == idcontr)
                    {
                        contracts[i].events.Add(events[events.Count - 1]);
                        events[events.Count - 1].contract = contracts[i];
                        flag = false;
                    }
                    i++;
                }

                
                
            }
        }
       /// <summary>
       /// метод для загрузки тарфиов из файла Tariff.csv
       /// </summary>
        private void LoadTariff()
        {
            String[] cont = File.ReadAllLines(savePath + "\\" + TariffFileName);
            foreach (String s in cont)
            {
                String[] arr = s.Split(';');
                Tariff tariff = new Tariff(Convert.ToInt32(arr[0]), Convert.ToInt32(arr[1]), Convert.ToInt32(arr[2]), Convert.ToInt32(arr[3]),arr[4]);
                tariffs.Add(tariff);
                int i = 0;
                bool flag = true;
                while (i < providers.Count&& flag)// добавление к провайдеру его тарифов
                {
                    if (providers[i].nameProv.Equals(arr[4]))
                    {
                        providers[i].tariffs.Add(tariff);
                        flag = false;
                    }
                    i++;
                }
                

               
            }
        }
        /// <summary>
        /// метод для загрузки клиентов из файла Client.csv
        /// </summary>
        private void LoadClient()
        {
             String[] cont = File.ReadAllLines(savePath + "\\" + ClientFileName);
             foreach (String s in cont)
             {
                 String[] arr = s.Split(';');
                 clients.Add(new Client(Convert.ToInt32(arr[0]), arr[1], arr[2], arr[3],arr[4],arr[5]));


             }

        }
/// <summary>
/// метод для загрузки данных из файлов.
/// </summary>
        public void Load()
        {
            LoadProviders();
            LoadClient();
            LoadTariff();
            LoadContracts();
            LoadEvents();
            
        }
        /// <summary>
        /// метод для сохранения данных в файлы
        /// </summary>
        public void Save()
        {
            SaveEvents();
            SaveClient();
            SaveTariff();
            SaveContracts();
            SaveProvider();

        }

        private void SaveProvider()
        {
            String[] arr = new String[providers.Count ];
            for (int i = 0; i < providers.Count; i++)
            {

                arr[i] += providers[i].nameProv;
            }
            File.WriteAllLines(ProvidersFileName, arr);
        }

        private void SaveContracts()
        {
             String[] arr = new String[contracts.Count ];
            for (int i = 0; i < contracts.Count; i++)
            {
                string s;
                s = Convert.ToString(contracts[i].bill) + ";" + Convert.ToString(contracts[i].idcontract) + ";" + contracts[i].passwd + ";" + contracts[i].phonenumber + ";" + contracts[i].provider.nameProv + ";" + Convert.ToString(contracts[i].client.idclient) + ";" + Convert.ToString(contracts[i].tariff.idtariff) + ";" + contracts[i].dateconclude+";" + contracts[i].podklcall+";" + contracts[i].podklsms+";" + contracts[i].podklinternet;
            arr[i] += s;
            }
            File.WriteAllLines(ContractsFileName, arr);
        }

        private void SaveTariff()
        {
            String[] arr = new String[tariffs.Count ];
            for (int i = 0; i < tariffs.Count; i++)
            {
                string s;
                s =  Convert.ToString(tariffs[i].idtariff)+";"+Convert.ToString(tariffs[i].smscost)+";"+Convert.ToString(tariffs[i].mincost)+";"+Convert.ToString(tariffs[i].mbcost)+";"+tariffs[i].nameProv;
                arr[i] += s;
            }
            File.WriteAllLines(TariffFileName, arr);
        }
        /// <summary>
        /// 
        /// </summary>
        private void SaveClient()
        {
            String[] arr = new String[clients.Count ];
            for (int i = 0; i < clients.Count; i++)
            {
                string s;
                s = Convert.ToString(clients[i].idclient) + ";" + clients[i].surname + ";" + clients[i].name + ";" + clients[i].otchestvo + ";" + clients[i].adress+ ";" + clients[i].password;
                arr[i] += s;
            }
            File.WriteAllLines(ClientFileName, arr, Encoding.UTF8);
        }
        /// <summary>
        /// 
        /// </summary>
        private void SaveEvents()
        {
            String[] arr = new String[events.Count ];
            for (int i = 0; i < events.Count; i++)
            {
                string s;
                s = events[i].date + ";" + events[i].service + ";" + events[i].volume + ";" + events[i].subnumber + ";" + events[i].cost + ";" + Convert.ToString(events[i].contract.idcontract);
                arr[i] += s;
            }
            File.WriteAllLines(EventsFileName, arr);
        }
        /// <summary>
        /// Метод для добавления провайдера
        /// </summary>
        /// <param name="nameNewProv">Имя создаваемого провайдера</param>
        /// <returns>результат выполнения операции</returns>
        public string AddProvider(string nameNewProv)
        {
            string result = null;
            
            if (!(  String.IsNullOrWhiteSpace(nameNewProv)))
            {
                int indexprov = ExistProvider(nameNewProv);
                if (indexprov != -1)
                {
                    providers.Add(new Provider(nameNewProv));
                    result = dobavleno;
                }
                else result = providerexist;

            }
            else
                result = zapvsepola;

            return result;
        }
        /// <summary>
        /// Метода добавления клиента
        /// </summary>
        /// <param name="surname">Фамилия создаваемого клиетна</param>
        /// <param name="name">Имя создаваемого клиетна</param>
        /// <param name="otchestvo">Отчество создаваемого клиетна</param>
        /// <param name="adress">Адрес проживания создаваемого клиетна</param>
        /// <param name="passwd">пароль создаваемого клиетна</param>
        /// <returns></returns>
        public string AddClient (string surname, string name, string otchestvo,string adress,string passwd)
        {
            string result=dobavleno;
            if (!(String.IsNullOrWhiteSpace(surname) || String.IsNullOrWhiteSpace(name) || String.IsNullOrWhiteSpace(otchestvo) || adress.Equals(emptadr) || String.IsNullOrWhiteSpace(passwd)) && !adress.Contains(substrtownstreet) && !adress.Substring(adress.Length - 5, 5).Equals(substrtownhome) && !adress.Contains(substrstreetnhome))
            {
                string chekersurn = surname;
                string chekernam = name;
                string chekerotch = otchestvo;
                chekersurn = System.Text.RegularExpressions.Regex.Replace(chekersurn, @"[^a-zA-Zа-яА-Я]", string.Empty);
                chekernam = System.Text.RegularExpressions.Regex.Replace(chekernam, @"[^a-zA-Zа-яА-Я]", string.Empty);
                chekerotch = System.Text.RegularExpressions.Regex.Replace(chekerotch, @"[^a-zA-Zа-яА-Я]", string.Empty);
                if (chekersurn.Equals(surname) && chekernam.Equals(name) && chekerotch.Equals(otchestvo))
                {
                    clients.Add(new Client(clients[clients.Count - 1].idclient + 1, surname, name, otchestvo, adress, passwd));
                }
                else result = invalidsimv;
                
            }
            else
            {
                result = zapvsepola;
            }


            return result;
        }
        /// <summary>
        ///  Метод добавления тарифа
        /// </summary>
        /// <param name="smscost">Стоимость смс нового тарифа</param>
        /// <param name="mincost">Стоимость одной минуты исходящего звонка нового тарифа</param>
        /// <param name="mbcost">Стоимость одного мегабайта нового тарифа</param>
        /// <param name="provider">Провайдер нового тарифа</param>
        /// <returns></returns>
        public string AddTariff(string smscost, string mincost, string mbcost, string provider)
        {

            string result=yes;

            if (!(String.IsNullOrWhiteSpace(smscost) || String.IsNullOrWhiteSpace(mincost) || String.IsNullOrWhiteSpace(mbcost) || String.IsNullOrWhiteSpace(provider)))
            {
                bool b = true;
                int i = 0;
                while (i < tariffs.Count &&b )
                {
                    if ((Convert.ToInt32(smscost) == tariffs[i].smscost) && (Convert.ToInt32(mincost) == tariffs[i].mincost) && (Convert.ToInt32(mbcost) == tariffs[i].mbcost) && (provider == tariffs[i].nameProv))
                    {
                        b = false;
                        
                    }
                    i++;
                }
                if (b)
                {
                     i = 0;
                    while ( i < providers.Count&&!result.Equals(dobavleno) )
                    {
                        string s = providers[i].nameProv;
                        string m = provider;
                        if (s.Equals(m))
                        {
                            Tariff tariff = new Tariff(tariffs[tariffs.Count - 1].idtariff + 1, Convert.ToInt32(smscost), Convert.ToInt32(mincost), Convert.ToInt32(mbcost), provider);
                            tariffs.Add(tariff);
                            providers[i].tariffs.Add(tariff);
                            result = dobavleno;
                        }
                        else result = providernotexist;
                        i++;
                    }                   

                }
                else
                    result = tariffexist;
            }

            else result = zapvsepola;
           
                    return result;
        }
  
      
        /// <summary>
        /// Метод для добаления контракта
        /// </summary>
        /// <param name="passwd">пароль контракта</param>
        /// <param name="idtariff">id подключаемого тарифа</param>
        /// <param name="dateconclude">дата заключения договора</param>
        /// <param name="idclient">id клиета, которому принадлежит договор</param>
        /// <param name="podcall">наличие услуги звонка</param>
        /// <param name="podsms">наличие услуги смс</param>
        /// <param name="podinternet">наличие улуги интернет</param>
        /// <returns>результат о выполнении операции</returns>
        public string AddContract(string passwd,string idtariff,string dateconclude,string idclient,string podcall,string podsms,string podinternet)
        {
            int indextariff = ExistTariff(idtariff);
            int indexClient = ExistClient(idclient);
            string result=zapvsepola;
            if (!(String.IsNullOrWhiteSpace(passwd) || String.IsNullOrWhiteSpace(idtariff) || String.IsNullOrWhiteSpace(dateconclude)))
            {

                if (indextariff!=-1)
                {
                    
                    if (indexClient!=-1)
                    {
                        int j = 0;
                        string newnumb = (Convert.ToInt64(contracts[contracts.Count - 1].phonenumber) + 1).ToString();
                        while(ExistContract(newnumb)!=-1)
                        {
                            newnumb = (Convert.ToInt64(newnumb) + 1).ToString();
                            j++;
                        }
                        contracts.Add(new Contract(0, contracts[contracts.Count - 1].idcontract + 1, passwd, newnumb, dateconclude,podcall,podsms,podinternet));
                        string Provname = tariffs[indextariff].nameProv;
                        LinkContractvsall(Provname, idclient, idtariff);
                        result = dobavleno;
                    }
                    else result = clientnotexist;
                }
                else result = tariffnotexist;
            }
            else result = zapvsepola;

            return result;
        }

        
        /// <summary>
        /// метод для добавления события
        /// </summary>
        /// <param name="date"> дата совершенного события</param>
        /// <param name="service">используемый сервис</param>
        /// <param name="volume">объем переданных данных</param>
        /// <param name="number">номер абонета</param>
        /// <param name="idcontract">индитификационный номер провайдера</param>
        /// <returns></returns>
        public string AddEvent(string date, string service, string volume, string number,string idcontract)
        {
            string result = yes;
            int i = 0;
            if (!(String.IsNullOrWhiteSpace(date) || String.IsNullOrWhiteSpace(service) || String.IsNullOrWhiteSpace(number) || String.IsNullOrWhiteSpace(idcontract)))
            {
                if (service.Equals(ctype) || service.Equals(itype) || service.Equals(stype))
                {
                    int idcontr = Convert.ToInt32(idcontract);
                    while (i < contracts.Count && !result.Equals(dobavleno) && !result.Equals(notenableserv))
                    {
                        if (contracts[i].idcontract == idcontr)
                        {
                            bool callenabled = (service.Equals(ctype) && contracts[i].podklcall.Equals(yes)),
                                smsenabled = (service.Equals(stype) && contracts[i].podklsms.Equals(yes)),
                                internetenabled = (service.Equals(itype) && contracts[i].podklinternet.Equals(yes));
                            if (callenabled || smsenabled || internetenabled)
                            {
                                int cost = ((callenabled) ? contracts[i].tariff.mincost : (smsenabled) ? contracts[i].tariff.smscost : (internetenabled) ? contracts[i].tariff.mbcost : 0);


                                contracts[i].ChangeBill(-Convert.ToDouble(volume) * cost);
                                events.Add(new Event(date, service, volume, number, (Convert.ToDouble(volume) * cost).ToString()));
                                contracts[i].events.Add(events[events.Count - 1]);
                                events[events.Count - 1].contract = contracts[i];
                                result = dobavleno;
                            }
                            else
                            {
                                result = notenableserv;
                            }
                        }
                        else result = contrisnotexist;
                        i++;                        
                    }

                }
                else result = notexistserv;
                
            }
            else result = zapvsepola;
            return result;
        }
     /// <summary>
     /// Метод для удаления провайдера
     /// </summary>
     /// <param name="Provname">имя удаляемого провайдера</param>
     /// <returns>результат о выполнении операции</returns>
        public string DelProvider(string Provname)
        {
          string result=deleted;
          int indexprov=ExistProvider(Provname);
          if (indexprov != -1)
          {
              if (providers.Count > 1)
              {

                  int i = 0;
                  int provcount = providers.Count;
                  while (i < contracts.Count)
                  {
                      if (contracts[i].provider.nameProv.Equals(Provname))
                          DelContract(contracts[i].phonenumber.ToString());
                      if (providers.Count == provcount)
                          i++;
                      else provcount=providers.Count;
                  }
                  i = 0;
                  int tarifcount = tariffs.Count;
                  while (i < tariffs.Count)
                  {
                      if (tariffs[i].nameProv.Equals(Provname))
                          DelTariff(tariffs[i].idtariff.ToString());
                      if (tarifcount == tariffs.Count)
                          i++;
                      else tarifcount=tariffs.Count;
                  }
                  providers.Remove(providers[indexprov]);
              }
              else result = cantdelet;
          }
          else result = providernotexist;

          return result;
        }
        /// <summary>
        /// Метод для удаления тарффа
        /// </summary>
        /// <param name="idtariff">id удаляемого тарифа</param>
        /// <returns>результат о выполнении операции</returns>
      public string DelTariff(string idtariff)
      {
          string result=deleted;
          int idtariffint=Convert.ToInt32(idtariff);
          int indextariff = ExistTariff(idtariff);
          int indexprov = ExistProvider(tariffs[indextariff].nameProv);

          int i=0;
          int j = 0;
          if (indextariff != -1)
          {
              int indexprovider = ExistProvider(tariffs[indextariff].nameProv);
              if (providers[indexprovider].tariffs.Count > 1)
              {
                  if (providers[indexprovider].tariffs[providers[indexprovider].tariffs.Count - 1].idtariff == idtariffint)
                  {
                      
                        while(j<contracts.Count)
                      {
                          contracts[j].tariff = providers[indexprovider].tariffs[providers[indexprovider].tariffs.Count - 2];
                          j++;
                      }
                  }else
                  {

                      while (j < contracts.Count)
                      {
                          contracts[j].tariff = providers[indexprovider].tariffs[providers[indexprovider].tariffs.Count - 1];
                          j++;
                      }
                  }
                  tariffs.Remove(tariffs[indextariff]);

              }
              else
              {
                  while(i<contracts.Count)
                  {
                      if (contracts[i].tariff.idtariff == idtariffint)
                          DelContract(contracts[i].phonenumber.ToString());
                  }
                  tariffs.Remove(tariffs[indextariff]);
                  i=0;
                  while(i<providers[indexprovider].tariffs.Count)
                  {
                      if (providers[indexprovider].tariffs[i].idtariff == idtariffint)
                          providers[indexprovider].tariffs.Remove(providers[indexprovider].tariffs[i]);
                      else
                          i++;
                  }

              }
          }
          else result = tariffnotexist;

          return result;
          
      }
        /// <summary>
      /// Метод для удаления контракта
        /// </summary>
        /// <param name="phonenumb">телефонный номер удаляемого контракта</param>
      /// <returns>результат о выполнении операции</returns>
      public string DelContract(string phonenumb)
      {
          string result=deleted;
          int indexcontract = ExistContract(phonenumb);
          int indexprovider = ExistProvider(contracts[indexcontract].provider.nameProv);
          int indexclient = ExistClient(contracts[indexcontract].client.idclient.ToString());

          int i = 0;
          if (indexcontract != -1)
          {
              int evcount = events.Count;
              while(i<events.Count)
                  {
                    if(events[i].contract.phonenumber.Equals(phonenumb))
                        {
                            events.Remove(events[i]);
                        }
                    if (evcount == events.Count)
                        i++;
                    else evcount = events.Count;

              }
              
              contracts.Remove(contracts[indexcontract]);
              i = 0;
              while (i < providers[indexprovider].contracts.Count)
              {
                  if (providers[indexprovider].contracts[i].phonenumber.Equals(phonenumb))
                      providers[indexprovider].contracts.Remove(providers[indexprovider].contracts[i]);
                  else
                      i++;
              }
              i = 0;
              while (i < clients[indexclient].contracts.Count)
              {
                  if (clients[indexclient].contracts[i].phonenumber.Equals(phonenumb))
                      clients[indexclient].contracts.Remove(clients[indexclient].contracts[i]);
                  else
                      i++;
              }

          }
          else result = contrisnotexist;
          

          return result;
      }
        /// <summary>
      /// Метод для удаления клиента
        /// </summary>
        /// <param name="idclient">id удаляемого клиента</param>
      /// <returns>результат о выполнении операции</returns>
         public string DelClient(string idclient)
      {
          string result = deleted;
          int i = 0;
          int indexclient = ExistClient(idclient);
          if (indexclient != -1)
          {
              int cocount = contracts.Count;
              while (i < contracts.Count)
              {
                  if (contracts[i].client.idclient.ToString().Equals(idclient))
                      DelContract(contracts[i].phonenumber.ToString());
                  if (contracts.Count == cocount)
                      i++;
                  else cocount = providers.Count;
              }
              clients.Remove(clients[indexclient]);
          }
          else result = clientnotexist;

          return result;
      }
        private void LinkContractvsall(string Provname,string idclient,string idtariff_1)
        {
            int i = 0;
            bool flag = true;
            while (i < providers.Count &&flag)
            {
                if (providers[i].nameProv.Equals(Provname))//связываем контракт с провайдером(добавляя контракт в список к провайдеру, и передаем кантракту провайдера)
                {
                    providers[i].contracts.Add(contracts[contracts.Count - 1]);
                    contracts[contracts.Count - 1].provider = providers[i];
                    flag = false;
                }
                i++;
            }
            i = 0;
            flag = true;
            while (i < clients.Count && flag)
            {
                if (clients[i].idclient == Convert.ToInt32(idclient))//связь клиента с контрактом(к клиенту добавляем его контракты(считаем что имя клиента уникально),в контракт добавляем клиента)
                {
                    clients[i].contracts.Add(contracts[contracts.Count - 1]);
                    contracts[contracts.Count - 1].client = clients[i];
                    flag = false;
                }
                i++;
            }
            int idtariff = Convert.ToInt32(idtariff_1);// добавление к контракту его тарифа
            i = 0;
            flag = true;
            while (i < tariffs.Count && flag)
            {
                if (tariffs[i].idtariff == idtariff)
                {

                    contracts[contracts.Count - 1].tariff = tariffs[i];
                    flag = false;
                }
                i++;
            }
        }
        public int ExistClient(string idclient)
        {
            int indexclient = -1;
            int i = 0; 
            while ( i < clients.Count&&(indexclient==-1))
                {
                    if (clients[i].idclient == Convert.ToInt32(idclient))
                      indexclient=i;    
                    i++;
                }
            return indexclient;
        }
        public int ExistContract(string phonenumb)
      {
          int indexcontract = -1;
          int i = 0;

          while (i < contracts.Count && (indexcontract == -1))
          {
              if (contracts[i].phonenumber.Equals(phonenumb))
                  indexcontract = i;

              i++;
          }
          return indexcontract;
      }        
        private int ExistProvider(string Provname)
        {
            int indexprov = -1;
          int i=0;

          while (i<providers.Count&& (indexprov==-1))
          {
              if (providers[i].nameProv.Equals(Provname))
                  indexprov = i;

              i++;
          }
            return indexprov;
        }        
      public int ExistTariff(string idtariff)
        {
            int indextariff = -1;
            int i = 0;
            while (i < tariffs.Count&&(indextariff==-1) )
                {
                    if (tariffs[i].idtariff == Convert.ToInt32(idtariff))
                    {
                        indextariff=i;
                    }
                    i++;
                }
            return indextariff;

        }//функция проверки существования тарифа        
/// <summary>
      /// метод для изменения телефонного номера
/// </summary>
/// <param name="oldnum">старый номер мобильного    </param>
/// <param name="newnum">новый номер мобильного</param>
/// <returns>результат выполненеия операции</returns>
      public string ChangePhoneNumber(string oldnum,string newnum)
      {
          string result = ismeneno;
          int indexcontr = ExistContract(oldnum);
          int i=0;
          bool povtor = false;
          while (i < contracts.Count&&!povtor)
          {
              if (contracts[i].phonenumber.Equals(newnum))
                  povtor = true;
              
              i++;
          }
          if (!povtor)
          {
              if (newnum.Length == 11)
              {
                  i=0;
                  while(i<contracts[indexcontr].provider.contracts.Count)
                  {
                      if (contracts[indexcontr].provider.contracts[i].phonenumber.Equals(oldnum))
                          contracts[indexcontr].provider.contracts[i].phonenumber = newnum;
                      else i++;

                  }
                  i = 0;
                  while (i < contracts[indexcontr].client.contracts.Count)
                  {
                      if (contracts[indexcontr].client.contracts[i].phonenumber.Equals(oldnum))
                          contracts[indexcontr].client.contracts[i].phonenumber = newnum;
                      else i++;

                  }
                  
                  contracts[indexcontr].phonenumber = newnum;
                  result = ismeneno;
              }
              else result = mustbetensimb;
          }
          else result = cantchangenumb;
              return result;
      }

        public string Checkclient(string idclient ,string passwd)
      {

          string result=emtstr;
          int indexclient = ExistClient(idclient);
          if (indexclient != -1)
          {
              if (!clients[indexclient].password.Equals(passwd))
                  result=invalidclientpasswd;
          }
          else result = clientnotexist;

          return result;
      }

    }
}
