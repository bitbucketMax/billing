﻿namespace Billing
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdBack = new System.Windows.Forms.Button();
            this.cmdChangePinfo = new System.Windows.Forms.Button();
            this.cmdConclude = new System.Windows.Forms.Button();
            this.cmdContractChoose = new System.Windows.Forms.Button();
            this.txtContractPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.listContract = new System.Windows.Forms.ListView();
            this.lbselectcontract = new System.Windows.Forms.Label();
            this.lbres = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmdBack
            // 
            this.cmdBack.Location = new System.Drawing.Point(444, 12);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(140, 40);
            this.cmdBack.TabIndex = 0;
            this.cmdBack.Text = "Выйти из личного кабинета";
            this.cmdBack.UseVisualStyleBackColor = true;
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // cmdChangePinfo
            // 
            this.cmdChangePinfo.Location = new System.Drawing.Point(12, 12);
            this.cmdChangePinfo.Name = "cmdChangePinfo";
            this.cmdChangePinfo.Size = new System.Drawing.Size(139, 40);
            this.cmdChangePinfo.TabIndex = 1;
            this.cmdChangePinfo.Text = "Изменение личных данных клиента";
            this.cmdChangePinfo.UseVisualStyleBackColor = true;
            this.cmdChangePinfo.Click += new System.EventHandler(this.cmdChangePinfo_Click);
            // 
            // cmdConclude
            // 
            this.cmdConclude.Location = new System.Drawing.Point(200, 12);
            this.cmdConclude.Name = "cmdConclude";
            this.cmdConclude.Size = new System.Drawing.Size(150, 40);
            this.cmdConclude.TabIndex = 2;
            this.cmdConclude.Text = "Заключить новый  контракт";
            this.cmdConclude.UseVisualStyleBackColor = true;
            this.cmdConclude.Click += new System.EventHandler(this.cmdConclude_Click);
            // 
            // cmdContractChoose
            // 
            this.cmdContractChoose.Enabled = false;
            this.cmdContractChoose.Location = new System.Drawing.Point(352, 357);
            this.cmdContractChoose.Name = "cmdContractChoose";
            this.cmdContractChoose.Size = new System.Drawing.Size(200, 20);
            this.cmdContractChoose.TabIndex = 4;
            this.cmdContractChoose.Text = "Работа с выбранным контрактом";
            this.cmdContractChoose.UseVisualStyleBackColor = true;
            this.cmdContractChoose.Click += new System.EventHandler(this.cmdContractChoose_Click);
            // 
            // txtContractPassword
            // 
            this.txtContractPassword.Location = new System.Drawing.Point(147, 357);
            this.txtContractPassword.Name = "txtContractPassword";
            this.txtContractPassword.PasswordChar = '*';
            this.txtContractPassword.Size = new System.Drawing.Size(117, 20);
            this.txtContractPassword.TabIndex = 5;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(9, 357);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(120, 13);
            this.lblPassword.TabIndex = 6;
            this.lblPassword.Text = "ПАРОЛЬ КОНТРАКТА";
            // 
            // listContract
            // 
            this.listContract.FullRowSelect = true;
            this.listContract.Location = new System.Drawing.Point(12, 88);
            this.listContract.Name = "listContract";
            this.listContract.Size = new System.Drawing.Size(572, 243);
            this.listContract.TabIndex = 7;
            this.listContract.UseCompatibleStateImageBehavior = false;
            this.listContract.View = System.Windows.Forms.View.Details;
            this.listContract.SelectedIndexChanged += new System.EventHandler(this.listContract_SelectedIndexChanged);
            // 
            // lbselectcontract
            // 
            this.lbselectcontract.AutoSize = true;
            this.lbselectcontract.Location = new System.Drawing.Point(12, 72);
            this.lbselectcontract.Name = "lbselectcontract";
            this.lbselectcontract.Size = new System.Drawing.Size(106, 13);
            this.lbselectcontract.TabIndex = 8;
            this.lbselectcontract.Text = "Выберите контракт";
            // 
            // lbres
            // 
            this.lbres.AutoSize = true;
            this.lbres.Location = new System.Drawing.Point(144, 392);
            this.lbres.Name = "lbres";
            this.lbres.Size = new System.Drawing.Size(0, 13);
            this.lbres.TabIndex = 9;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 414);
            this.Controls.Add(this.lbres);
            this.Controls.Add(this.lbselectcontract);
            this.Controls.Add(this.listContract);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.txtContractPassword);
            this.Controls.Add(this.cmdContractChoose);
            this.Controls.Add(this.cmdConclude);
            this.Controls.Add(this.cmdChangePinfo);
            this.Controls.Add(this.cmdBack);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MainMenu";
            this.Text = "Главное меню";
            this.Activated += new System.EventHandler(this.MainMenu_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainMenu_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdBack;
        private System.Windows.Forms.Button cmdChangePinfo;
        private System.Windows.Forms.Button cmdConclude;
        private System.Windows.Forms.Button cmdContractChoose;
        private System.Windows.Forms.TextBox txtContractPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.ListView listContract;
        private System.Windows.Forms.Label lbselectcontract;
        private System.Windows.Forms.Label lbres;

    }
}