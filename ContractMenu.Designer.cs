﻿namespace Billing
{
    partial class ContractMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdBack = new System.Windows.Forms.Button();
            this.cmdDetail = new System.Windows.Forms.Button();
            this.cmdPay = new System.Windows.Forms.Button();
            this.cmdChange = new System.Windows.Forms.Button();
            this.cmdDelete = new System.Windows.Forms.Button();
            this.listContract = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // cmdBack
            // 
            this.cmdBack.Location = new System.Drawing.Point(376, 12);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(153, 23);
            this.cmdBack.TabIndex = 1;
            this.cmdBack.Text = "Вернуться в меню клиента";
            this.cmdBack.UseVisualStyleBackColor = true;
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // cmdDetail
            // 
            this.cmdDetail.Location = new System.Drawing.Point(125, 133);
            this.cmdDetail.Name = "cmdDetail";
            this.cmdDetail.Size = new System.Drawing.Size(99, 40);
            this.cmdDetail.TabIndex = 2;
            this.cmdDetail.Text = "Детализация";
            this.cmdDetail.UseVisualStyleBackColor = true;
            this.cmdDetail.Click += new System.EventHandler(this.cmdDetail_Click);
            // 
            // cmdPay
            // 
            this.cmdPay.Location = new System.Drawing.Point(15, 133);
            this.cmdPay.Name = "cmdPay";
            this.cmdPay.Size = new System.Drawing.Size(104, 40);
            this.cmdPay.TabIndex = 3;
            this.cmdPay.Text = "Пополнение счета";
            this.cmdPay.UseVisualStyleBackColor = true;
            this.cmdPay.Click += new System.EventHandler(this.cmdPay_Click);
            // 
            // cmdChange
            // 
            this.cmdChange.Location = new System.Drawing.Point(310, 133);
            this.cmdChange.Name = "cmdChange";
            this.cmdChange.Size = new System.Drawing.Size(219, 40);
            this.cmdChange.TabIndex = 4;
            this.cmdChange.Text = "Изменение данных договора";
            this.cmdChange.UseVisualStyleBackColor = true;
            this.cmdChange.Click += new System.EventHandler(this.cmdChange_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Location = new System.Drawing.Point(125, 192);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(279, 23);
            this.cmdDelete.TabIndex = 6;
            this.cmdDelete.Text = "Расторжение договора";
            this.cmdDelete.UseVisualStyleBackColor = true;
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // listContract
            // 
            this.listContract.Location = new System.Drawing.Point(0, 55);
            this.listContract.Name = "listContract";
            this.listContract.Size = new System.Drawing.Size(541, 72);
            this.listContract.TabIndex = 7;
            this.listContract.UseCompatibleStateImageBehavior = false;
            this.listContract.View = System.Windows.Forms.View.Details;
            // 
            // ContractMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 240);
            this.Controls.Add(this.listContract);
            this.Controls.Add(this.cmdDelete);
            this.Controls.Add(this.cmdChange);
            this.Controls.Add(this.cmdPay);
            this.Controls.Add(this.cmdDetail);
            this.Controls.Add(this.cmdBack);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ContractMenu";
            this.Text = "Контракт";
            this.Activated += new System.EventHandler(this.ContractMenu_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ContractMenu_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cmdBack;
        private System.Windows.Forms.Button cmdDetail;
        private System.Windows.Forms.Button cmdPay;
        private System.Windows.Forms.Button cmdChange;
        private System.Windows.Forms.Button cmdDelete;
        private System.Windows.Forms.ListView listContract;

    }
}