﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
   public class Provider
    {
        public string nameProv;
        public List<Tariff> tariffs= new List<Tariff>();
        public List<Contract> contracts=new List<Contract>();
        public Provider(string nameProv)
        {
            this.nameProv = nameProv;
        }
    }
}
