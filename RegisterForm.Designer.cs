﻿namespace Billing
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFamiliya = new System.Windows.Forms.TextBox();
            this.txtTown = new System.Windows.Forms.TextBox();
            this.lblFamiliya = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.cmdRegister = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.txtStreet = new System.Windows.Forms.TextBox();
            this.txtHome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtOtchestvo = new System.Windows.Forms.TextBox();
            this.lbName = new System.Windows.Forms.Label();
            this.lblOtchestvo = new System.Windows.Forms.Label();
            this.txtpasswd = new System.Windows.Forms.TextBox();
            this.lbpasswd = new System.Windows.Forms.Label();
            this.lbresult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtFamiliya
            // 
            this.txtFamiliya.Location = new System.Drawing.Point(113, 12);
            this.txtFamiliya.Name = "txtFamiliya";
            this.txtFamiliya.Size = new System.Drawing.Size(209, 20);
            this.txtFamiliya.TabIndex = 0;
            // 
            // txtTown
            // 
            this.txtTown.Location = new System.Drawing.Point(113, 147);
            this.txtTown.Name = "txtTown";
            this.txtTown.Size = new System.Drawing.Size(209, 20);
            this.txtTown.TabIndex = 1;
            // 
            // lblFamiliya
            // 
            this.lblFamiliya.AutoSize = true;
            this.lblFamiliya.Location = new System.Drawing.Point(53, 12);
            this.lblFamiliya.Name = "lblFamiliya";
            this.lblFamiliya.Size = new System.Drawing.Size(56, 13);
            this.lblFamiliya.TabIndex = 2;
            this.lblFamiliya.Text = "Фамилия";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(12, 132);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(38, 13);
            this.lblAddress.TabIndex = 3;
            this.lblAddress.Text = "Адрес";
            // 
            // cmdRegister
            // 
            this.cmdRegister.Location = new System.Drawing.Point(53, 273);
            this.cmdRegister.Name = "cmdRegister";
            this.cmdRegister.Size = new System.Drawing.Size(124, 39);
            this.cmdRegister.TabIndex = 4;
            this.cmdRegister.Text = "Зарегистрироваться";
            this.cmdRegister.UseVisualStyleBackColor = true;
            this.cmdRegister.Click += new System.EventHandler(this.cmdRegister_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(187, 273);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(135, 39);
            this.cmdCancel.TabIndex = 5;
            this.cmdCancel.Text = "Отмена";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // txtStreet
            // 
            this.txtStreet.Location = new System.Drawing.Point(113, 173);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(209, 20);
            this.txtStreet.TabIndex = 6;
            // 
            // txtHome
            // 
            this.txtHome.Location = new System.Drawing.Point(113, 199);
            this.txtHome.Name = "txtHome";
            this.txtHome.Size = new System.Drawing.Size(209, 20);
            this.txtHome.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Город";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Улица";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(79, 199);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Дом";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(113, 38);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(209, 20);
            this.txtName.TabIndex = 0;
            // 
            // txtOtchestvo
            // 
            this.txtOtchestvo.Location = new System.Drawing.Point(113, 64);
            this.txtOtchestvo.Name = "txtOtchestvo";
            this.txtOtchestvo.Size = new System.Drawing.Size(209, 20);
            this.txtOtchestvo.TabIndex = 0;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(51, 38);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(29, 13);
            this.lbName.TabIndex = 2;
            this.lbName.Text = "Имя";
            // 
            // lblOtchestvo
            // 
            this.lblOtchestvo.AutoSize = true;
            this.lblOtchestvo.Location = new System.Drawing.Point(53, 64);
            this.lblOtchestvo.Name = "lblOtchestvo";
            this.lblOtchestvo.Size = new System.Drawing.Size(54, 13);
            this.lblOtchestvo.TabIndex = 2;
            this.lblOtchestvo.Text = "Отчество";
            // 
            // txtpasswd
            // 
            this.txtpasswd.Location = new System.Drawing.Point(113, 90);
            this.txtpasswd.Name = "txtpasswd";
            this.txtpasswd.Size = new System.Drawing.Size(209, 20);
            this.txtpasswd.TabIndex = 0;
            // 
            // lbpasswd
            // 
            this.lbpasswd.AutoSize = true;
            this.lbpasswd.Location = new System.Drawing.Point(50, 90);
            this.lbpasswd.Name = "lbpasswd";
            this.lbpasswd.Size = new System.Drawing.Size(45, 13);
            this.lbpasswd.TabIndex = 2;
            this.lbpasswd.Text = "Пароль";
            // 
            // lbresult
            // 
            this.lbresult.AutoSize = true;
            this.lbresult.Location = new System.Drawing.Point(113, 226);
            this.lbresult.Name = "lbresult";
            this.lbresult.Size = new System.Drawing.Size(0, 13);
            this.lbresult.TabIndex = 11;
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 324);
            this.Controls.Add(this.lbresult);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtHome);
            this.Controls.Add(this.txtStreet);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdRegister);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lbpasswd);
            this.Controls.Add(this.lblOtchestvo);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.lblFamiliya);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.txtpasswd);
            this.Controls.Add(this.txtOtchestvo);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtFamiliya);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "RegisterForm";
            this.Text = "Регистрация";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegisterForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFamiliya;
        private System.Windows.Forms.TextBox txtTown;
        private System.Windows.Forms.Label lblFamiliya;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Button cmdRegister;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.TextBox txtStreet;
        private System.Windows.Forms.TextBox txtHome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtOtchestvo;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lblOtchestvo;
        private System.Windows.Forms.TextBox txtpasswd;
        private System.Windows.Forms.Label lbpasswd;
        private System.Windows.Forms.Label lbresult;
    }
}